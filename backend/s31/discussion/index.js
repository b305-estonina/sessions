// JavaScript Object Notation (JSON)

/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/

// JSON Arrays
// Array of JSON Objects

/*"cities" : [
		{"city" : "Quezon City", "province" : "Metro Manila", "country" : "Philippines"},
		{"city" : "Manila City", "province" : "Metro Manila", "country" : "Philippines"},
		{"city" : "Makati City", "province" : "Metro Manila", "country" : "Philippines"}	
]*/

// JSON Methods

let batchesArray = [{batchName : "Batch X"}, {batchName: "Batch Y"}];

// Stringify method

console.log("Result from stringify:");
console.log(JSON.stringify(batchesArray));

let data = JSON.stringify({
	name : "John",
	age: 31,
	address: {
		city : "Manila",
		country: "Philippines"
	}
});

console.log(data);

// Using stringify method with variables

let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city : prompt("Which city do you live in?"),
	country : prompt("Which country does your city address belong to?")
}

let otherData = JSON.stringify({
	firstName : firstName,
	lastName : lastName,
	age : age,
	address : address
})

console.log(otherData);

// Convert stringified JSON into JS objects
// parse()

let batchesJSON = '[{"batchName" : "Batch X"}, {"batchName" : "Batch Y"}]'

console.log("Result from parse:")
console.log(JSON.parse(batchesJSON));