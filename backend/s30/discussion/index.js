// Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals

let name = "John";

// backticks (``)

let message = `Hello ${name}! Welcome to Programming!`;
console.log(message);

// Multi-line using Template Literals

let anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;

console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// Array Destructuring

let fullName = ["Juan", "Dela", "Cruz"];

const [firstName, middleName, lastName] = fullName;

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// Object Destructuring

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Destructuring an object

const {givenName, maidenName, familyName} = person;
console.log(`${givenName} ${maidenName} ${familyName}`);

// Arrow Function

// function hello(){
// 	console.log("Hello World!");
// }

const hello = () => {
	console.log("Hello World!");
}

const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`);
};

printFullName("John", "D", "Smith");

// Arrow Function with Loops

let students = ["John", "Jane", "Judy"];

students.forEach((student) => {
	console.log(`${student} is a student.`)
});

// Implicit Return Statement

const add = (x, y) => {
	return x + y
}

let total = add(1, 2);
console.log(total);

// Implicit Return

const sum = (x, y) => x+y;

let total2 = sum(5, 10);
console.log(total2);

// Class - Based Objects Blueprints

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instance = duplicate = copy

const myCar = new Car();

console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = "2021";

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", "2021");
console.log(myNewCar)