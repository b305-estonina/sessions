// [ SECTION ] Dependencies and Modules
const jwt = require('jsonwebtoken');

const secret = "CourseBookingAPI";

// token creation
module.exports.createAccessToken = (user) => {
	
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {}); // eto yung mismong token na ibibigay
};


module.exports.verify = (req, res, next) => {

	// Token is retrieved from the request header
	// Authorization (Auth Tab) > Bearer Token

	console.log(req.headers.authorization);

	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth: "Failed, No Token!"})
	}else{
		console.log(token);

		/*
		 Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxZjBjMzYyNmNhYzJjM2VhYTBmY2I5YSIsImVtYWlsIjoic3BpZGVybWFuM0BnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjQzMjQ1MTEyfQ.c29qelk9GkrnZP10M6wqo6fiTKHPk-c15DcpSBsKq7I 

		*/

		token = token.slice(7, token.length);

		console.log(token);

		jwt.verify(token, secret, function(error, decodedToken){
			if(error){
				return res.send({
					auth: "Failed",
					message: error.message // default error message
				});
			}else{
				console.log(decodedToken);
				// Contains data from our token

				req.user = decodedToken;

				next();
				// Will let us proceed to the next controller ex. pag naglogin deretso sa homepage, hindi na papakita ung token/ or yung pagverify, (login-verify-homepage)'we dont want the user to access our tokens'
			}
		})
	}
}


// Verify if user account is admin or not

module.exports.verifyAdmin = (req, res, next) =>{

	if(req.user.isAdmin){
		next()
	}else{
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}


}
