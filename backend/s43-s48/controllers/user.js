	// [ SECTION ] Dependecies and Modules
const User = require('../models/User');
const Course = require("../models/Course.js");
const bcrypt = require('bcrypt');

const auth = require('../auth');

// check email exists
/*
	Steps:
		1. Use mongoose 'find' method to find duplicate emails
		2. Use the 'then' method to send a response back to the frontend application based on the result of the "find" method.
*/
module.exports.checkEmailExist = (reqBody) => {
	console.log(reqBody);
	console.log(reqBody.email);
	return User.find({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result.length > 0){
			return true
		} else {
			return false
		};
	});
};

// user registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
*/
module.exports.registerUser = (reqBody) => {
	console.log(reqBody);
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
	.catch(err => err); // saluhin ang mga error
};

// user authentication
//module.exports.loginUser = (req, res) => {
module.exports.loginUser = (reqBody) => {

	//return User.findOne({ email: req.body.email }).then(result => {
	return User.findOne({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result === null){
			//return res.send(false); // the email doesn't exist in our DB
			return false; // the email doesn't exist in our DB
		} else {

			//const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				//return res.send({ access: auth.createAccessToken(result)})
				return { access: auth.createAccessToken(result)}
			} else {
				return false; // Passwords do not match
			}
		}
	})
	// .catch(err => res.send(err))
}

// Retrieve user details

/*module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id).then(result => {
		result.password = "*******";
		return (result);
	})
	.catch(error => error)
}*/

module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "********";
		return res.send(result);
	})
	.catch(error => error)
}


module.exports.enroll = async (req, res) => {
	console.log(req.user.id);
	console.log(req.body.courseId);

	if(req.user.isAdmin){
		return res.send("You cannot do this action.")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		let newEnrollment = {
			courseId: req.body.courseId,

		}

		let listOfEnrollments = user.enrollments;

		let filteredCourseId = [];

		listOfEnrollments.forEach((result) => {
			filteredCourseId.push(result.courseId);
		});

		console.log(filteredCourseId);

		let enrolmentDuplicate = filteredCourseId.includes(req.body.courseId);

		console.log("enrollment duplicate: " + enrolmentDuplicate);

		if(enrolmentDuplicate){
			return res.send("You are already enrolled for this course.");
		}

			user.enrollments.push(newEnrollment);


		return user.save().then(user => true).catch(error => res.send(error));
		//implicit return, nd na kelangan ilagay na word na return, pag na save true, pag hindi catch ng error
	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(error => res.send(error));
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send("You are now enrolled to a course. Thank you!");
	}
}

module.exports.getEnrollments = (req, res) => {
	User.findById(req.user.id).then(result => {
		if(result.enrollments == [] || result.enrollments == null){
			return res.send("You are not enrolled to any courses.")
		}else{
			res.send(result.enrollments);
		}
	}).catch(error => res.send(error));
}


module.exports.resetPassword = async (req, res) => {
	try{
		const newPassword = req.body.newPassword;
		const userId = req.user.id;

		// Hashing the new password
		const hashedPassword = await bcrypt.hash(newPassword, 10);

		await User.findByIdAndUpdate(userId, {password: hashedPassword});

		res.status(200).json({message: "Password reset successfully!"});
	}catch(error){
		res.status(500).json({message: "Internal Server Error"});
	}
}
