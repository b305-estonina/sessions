// CRUD Operations (MongoDB)

// CRUD Operations are the heart of any backend app.
// Mastering CRUD Operations is essential for any developers.

/*

C - Create/Insert
R - Retrieve/Read
U - Update
D - Delete

*/

// [SECTION] Creating Documents

/*

SYNTAX: 

db.users.insertOne({object})

SQL:

INSERT INTO table_name (column1, column2, column3,....)
Values (values1, values2, values3....)

*/


db.users.insertOne({
	firstName:"Jane",
	lastName:"Doe",
	age:21,
	contact:{
		phone:"09123456789",
		email:"janedoe@gmail.com"
	},
	courses: ["CSS", "JS", "PYTHON"],
	department: "none"
});

db.users.insertOne({
	firstName:"Test",
	lastName:"Test",
	age:0,
	contact:{
		phone:"0000",
		email:"test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.insertOne({
	firstName:"Kane",
	lastName:"Estonina",
	age:32,
	contact:{
		phone:"09123456789",
		email:"enakestonina@gmail.com"
	},
	courses: ["CSS", "JS", "PYTHON"],
	department: "none"
});

/*

SYNTAX: 

db.users.insertMany([{object}])


*/

db.users.insertMany([
{
	firstName:"Stephen",
	lastName:"Hawking",
	age:76,
	contact:{
		phone:"09123456789",
		email:"stephenhawking@gmail.com"
	},
	courses: ["PHP", "React", "PYTHON"],
	department: "none"
},
{
	firstName:"Neil",
	lastName:"Armstrong",
	age:82,
	contact:{
		phone:"09123456789",
		email:"neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}
]);

// [SECTION] -Read/Retrieve

/*

SYNTAX:

db.users.findOne();
db.users.findOne({field: value});

*/

db.users.findOne();

db.users.findOne({firstName: "Stephen"});

/*

SYNTAX:

db.users.find();
db.users.find({field: value});

*/

db.users.find({department: "none"});

// multiple criteria

db.users.find({department: "none", age: 82});

// [SECTION] Updating a Data

/*

SYNTAX:

db.users.updateOne({criteria}, {$set: {field: value}});

*/

db.users.updateOne(
{firstName: "Test"},
{
	$set:{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact:{
			phone: "123456789",
			email: "billgates@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	}
}
);

// find the data
db.users.findOne({firstName: "Bill"});
db.users.findOne({firstName: "Test"});

db.users.find({"contact.email": "billgates@gmail.com"});

// Update many collection

/*

SYNTAX:

db.users.updateMany({criteria}, {$set: {field: value}});

*/

db.users.updateMany(
{department: "none"}, 
{
	$set: {
		department: "HR"
	}
}
);

// [SECTION] Deleting a data

db.users.insert({
	firstName: "Test"
});

// Delete single document

/*

SYNTAX:

db.users.deleteOne({criteria})

*/

db.users.deleteOne({
	firstName: "Test"
});

// Delete Many

/*

SYNTAX:

db.users.deleteMany({criteria})

*/

// Insert Another Bill
db.users.insert({
	firstName: "Bill"
});

db.users.deleteMany({firstName: "Bill"});