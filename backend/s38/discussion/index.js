let http = require('http');

let port = 4000;

let app = http.createServer(function(request, response){

	if(request.url === "/items" && request.method === "GET"){

		response.writeHead(200,{'Content-type': 'text/plain'});
		// Ends the response process
		response.end('Data retrieved from the database');
	}

	// The method "POST"
	if (request.url == "/items" && request.method === "POST" ){

		response.writeHead(200,{'Content-type': 'text/plain'});
		response.end('Data to be sent to the database');
	}
})

// Inform us if the server is running, by printing our message:
// First Arguement, the port number to assign the server
// Second Arguement, the callback/function to run when the server is running
app.listen(port, () => console.log ('Server is running at localhost:4000'))