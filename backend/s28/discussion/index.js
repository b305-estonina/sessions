// Non- Mutator Methods

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

//  indexOf();

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf " + invalidCountry);

// lastIndexOf()
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 3);
console.log("Result of lastIndexOf " + lastIndexStart);

//slice ()

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);

// toString()

let stringArray = countries.toString();
console.log(stringArray);

// concat()

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe sass"];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log(combinedTasks);

// join()

let users = ["Jhon", "Jane", "Joe", "Robert"];

console.log(users.join());
console.log(users.join(""));
console.log(users.join("-"));
console.log(users.join("x"));

// forEach()

tasks.forEach(function(task){
	console.log(task);
})

let filteredTasks = [];

tasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
})

console.log("Result of filtered task");
console.log(filteredTasks);

// every()

let numbers = [1, 2, 3, 4, 5];

let allValid = numbers.every(function(number){
	return (number < 3);
});

console.log(allValid);

// some()

let someValid = numbers.some(function(number){
	return (number < 3);
});

console.log(someValid);

// filter()

let filterValid = numbers.filter(function(number){
	return (number = 0)
});

console.log(filterValid);

// includes()

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// addtional example
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a")
});

console.log(filteredProducts)