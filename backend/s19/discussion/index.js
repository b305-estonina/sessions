console.log("Hello World?");

// [ SECTION ] Syntax, Statements and Comments
	// Js Statements usually end with semicolon (;)

	// Comments :

	// There are two types of comments
		// 1. The single-line comment denoted by //, double slash
		// 2. The multiple-line comment denoted by /**/, double and asterisk in th middle 

//  [ SECTION ] Variables
	// It is used to contain data
	// Any information that is used by an application is stored in what we call a "memory"

	// Declaring Variables

	// Declaring Variables - tells our devices that a variable name is created.

	let myVariableNumber; // A naming convension
	console.log("myVariableNumber");
	console.log(myVariableNumber);
	// let myVariableNumber
	// let my-variable-number
	// let my_variable_number

	// Syntax
		// let/const/var variableName

	// Declaring and Initializing Variables

	let productName = "desktop computer"; 
	console.log(productName);

	let productPrice1 = "18999";
	let productPrice = 18999
	console.log(productPrice1);
	console.log(productPrice);

	// const, impossible to reassign
	const interest = 3.539;
	// Reassigning variable values

	// float vs double
	// both can store decimal numbers
	// float -> 1.1234567
	// double -> 1.123456789012345

	// Reassigning variable values

	productName = "Laptop";
	console.log(productName)

	// interest = 4.239; // Will return an error
	// console.log(interest);

	// Reassigning Variables vs Initializing Variables

	// Declare the Variable First
	myVariableNumber = 2023;
	console.log(myVariableNumber)

	// Multiple Variable Declarations

	let productCode = 'DC017';
	const productBrand = 'Dell';

	console.log(productCode, productBrand);

	const hoursInADay = 24;
	const pie = 3.17

	// [ SECTION ] Data Types

		// Strings

		let country = "Philippines";
		let province = 'Metro Manila';

		// Concatenation Strings
		let fullAddress = province + ","
		+ country;
		console.log(fullAddress);
		// console.log("Metro Manila, Philippines");

		let greeting = "I live in the " + country;
		console.log(greeting);

		let mailAddress = 'Metro Manila \n\nPhilippines'
		console.log (mailAddress);

		let message = "John's employees went home early"
		console.log(message);

		message = 'John\'s employeees went home early again'
		console.log(message)

		// Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Numbers/Fractions
		let grade = 98.7;
		console.log(grade);

		//*Other Programing Language*/

		// Java
		// - System.out.printIn("Hello World")
		// C++
		// -cout<<"Hello World"
		// -printf("Hello World")
		// PHP
		// -echo "Hello World";
		// Js
		// -console.log("Hello World")




		// Exponential Notation
		// This is not related to, 2 raise to the power of 10
		let planetDistance = 2e10
		console.log(planetDistance)

		// Combining text and Strings

		console.log ("John's grade last quarter is " + grade)

		// Boolean
		// Returns true or false

		let isMarried = false
		let inGoodConduct = true

		console.log(isMarried);
		console.log(inGoodConduct);
		console.log("inGoodConduct: " + inGoodConduct);

		// Arrays
		let grades = [ 98.7, 92.1, 90.2, 94.6]
		console.log(grades);
		console.log(grades[0]);
		console.log(grades[1]);
		console.log(grades[2]);
		console.log(grades[3]);
		console.log(grades[4]);

		//Different data types 

		let details = [ "John", "Smith", 32, true]
		console.log(details);
		console.log(details[0])
		console.log(details[1])
		console.log(details[2])
		console.log(details[3])
		console.log(details[4])

		// Objects
		// Composed of "key/value pair"

		let person = {
			fullName: "Juan Dela Cruz",
			age: 35,
			isMarried: false,
			contact: ["09123456789", "09987654321"],
			address: {
				houseNumber: "345" ,
				city: "Manila"
			}
		};

		console.log(person);
		console.log(person.fullName)
		console.log(person.age);
		console.log(person.isMarried)
		//We only use [] to call the index number of the data from array 
		console.log(person.contact[0])
		console.log(person.contact[1])
		console.log(person.address.houseNumber)
		console.log(person.address.city)

		let arrays = [
			[ "hey", "hey1", "hey2",],
			[ "hey", "hey1", "hey2",],
			[ "hey", "hey1", "hey2",]			
		];

		console.log(arrays);

		let myGrades = {
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6,
		};

		console.log(myGrades)

		// Typeof Operator
			// It will determine the type of the data
		console.log(typeof myGrades);
		console.log(typeof arrays);
		console.log(typeof greeting);

		//  Null
		let spouse = null;
		let myNumber = 0;
		let myString = '';

		console.log(spouse)
		console.log(myNumber)
		console.log(myString)

		// Undefined
		let fullName;
		console.log(fullName);