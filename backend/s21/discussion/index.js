// console.log("Good evening!");

//[ SECTION ] Functions
    // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
    // Functions are mostly created to create complicated tasks to run several lines of code in succession
    // They are also used to prevent repeating lines/blocks of codes that perform the same task/function


//Function declarations
    //(function statement) defines a function with the specified parameters.

    /*
        Syntax:
        function functionName() {
            code block (statement)
        }
    */
    // function keyword - used to defined a javascript functions
    // functionName - the function name. Functions are named to be able to use later in the code.
    // function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

function printName(){
	console.log("My name is Janie");
};


//[ SECTION ] Function Invocation
    //The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
    //It is common to use the term "call a function" instead of "invoke a function".

 printName();

// declaredFunction(); - results in an error, much like variables, we cannot invoke a function we have yet to define. 

 //[ SECTION ] Function declarations vs expressions
     //Function Declarations

         //A function can be created through function declaration by using the function keyword and adding a function name.

         //Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).
         
         //declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

         //Note: Hoisting is Javascript's behavior for certain variables and functions to run or use them before their declaration.

 function declaredFunction() {
 	console.log("Hello World from declaredFunction()");
 };

 declaredFunction();

	 //Function Expression
	     //A function can also be stored in a variable. This is called a function expression.

	     //A function expression is an anonymous function assigned to the variableFunction

	     //Anonymous function - a function without a name.

 	// variableFunction(); - error (Uncaught ReferenceError: Cannot access 'variableFunction' before initialization)

 	let variableFunction = function(){
 		console.log("Hello again!");
 	};

 	variableFunction();

//[ SECTION ] Function scoping

/*  
    Scope is the accessibility (visibility) of variables.
    
    Javascript Variables has 3 types of scope:
        1. local/block scope
        2. global scope
        3. function scope
            JavaScript has function scope: Each function creates a new scope.
            Variables defined inside a function are not accessible (visible) from outside the function.
            Variables declared with var, let and const are quite similar when declared inside a function
*/  
 	 {
 	 	let localVar = "Shinichi Kudo";
 	 }

 	 let globalVar = "Son Goku";

 	 console.log(globalVar);
 	 // console.log(localVar); - error (Uncaught ReferenceError: localVar is not defined)

 	 function showNames(){

 	 	var functionVar = "Tony";
 	 	const functionConst = "Steve";
 	 	let functionLet = "Thor";

 	 	console.log(functionVar);
 	 	console.log(functionConst);
 	 	console.log(functionLet);

 	 	console.log(globalVar); // global variables can be called inside a local/block scope and function scope
 	 }

 	 showNames();

 	 // console.log(functionVar);
 	 // console.log(functionConst);
 	 // console.log(functionLet);

 	 //Nested Functions

 	       //You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

 	 function myNewFunction(){
 	 	let name = "Owen";

 	 	function nestedFunction(){
 	 		let nestedName = "Moon";
 	 		console.log(name);
 	 	}

 	 	// console.log(nestedName);

 	 	nestedFunction();
 	 }

 	 myNewFunction();
 	 // nestedFunction(); - error (index.js:126 Uncaught ReferenceError: nestedFunction is not defined)

 	 function myNewFunction2(){
 	 	myNewFunction();

 	 	console.log("This is new function 2");
 	 };

 	 myNewFunction2();

//[ SECTION ] Using alert()

	//alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

 	 // alert("Hello Batch 205!");

 	 function showSampleAlert(){
 	 	alert("Hello User!");
 	 };

 	 // showSampleAlert();

 	 console.log("I will only log in the console when the alert is dismissed");

 	/*
		Notes on using alert()
			* Show only an alert() for short dialogs/messages
			* Do not overuse alert() because the program has to wait for it to be dismissed
 	*/

//[ SECTION ] Using prompt()
 
	//prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

 	// let samplePrompt = prompt("Enter your name:");

 	// console.log("Hello " + samplePrompt);

 	/*
        Syntax:

        prompt("<dialogInString>");
    */

 	 // let sampleNullPrompt = prompt("Do not enter anything");

 	 // console.log(sampleNullPrompt); //prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

// [SECTION] RETURN STATEMENT
	// "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
	// "return" statement also stops the execution of the function and any code after the return statement will not be executed.

 	 function returnFunc(){
 	 	return "I am a return statement";
 	 }

 	 returnFunc(); // no display in the console

 	 function returnName(){

 	 	return "Owen" + " " + "Orange"
 	 	
 	 };

 	 // let name = returnName();
 	 // console.log(name);
 	 console.log(returnName());

//[ SECTION ] Function Naming Conventions
 //Function names should be definitive of the task it will perform. It usually contains a verb.

 	 function getCourses(){
 	 	let courses = ["Science 101", "Math 101", "English 101"];
 	 	return courses;
 	 }

 	 console.log(getCourses());

 //Avoid generic names to avoid confusion within your code. Use definitive names

 	 function get(){
 	 	let name = "Noir"
 	 	return name
 	 }

 	 console.log(get());

 	 //Avoid pointless and inappropriate function names.

 	     function foo(){

 	         console.log(25%5);

 	     };

 	     foo();

 	 //Name your functions in small caps. Follow camelCase when naming variables and functions.

 	     function displayCarInfo(){

 	         console.log("Brand: Toyota");
 	         console.log("Type: Sedan");
 	         console.log("Price: 1,500,000");

 	     }
 	     
 	     displayCarInfo();
