// Arrays

let grades = [98, 94, 99, 90];

// not recommended -> not a good practice
let mixedArr = ["John", false, {}, 15];

console.log(mixedArr);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// "length" method
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// "length" method will count the number of characters including empty space
let fullName = "Jamie Noble";
console.log(fullName.length);

let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

console.log(myTasks)
console.log(myTasks.length);

// use "length" method to delete array value

myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// Another way of deleting an array value
myTasks.length--;
console.log(myTasks);


let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

console.log(theBeatles[20]);

theBeatles[4] = "test";
console.log(theBeatles);

theBeatles[0] = "romenick";
console.log( theBeatles);

// Accessing the last index

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];

let lastIndexElement = lakersLegends.length - 1;

console.log(lakersLegends[lastIndexElement]);

let addIndexElement = lakersLegends.length;

lakersLegends[addIndexElement] = "test";
console.log(lakersLegends);

// adding elements in an array
let newArray = [];

newArray[0] = "Cloud Strife";
newArray[1] = "Tifa Lockhart";
newArray[newArray.length] = "Wallace";

newArray.push("test");

console.log(newArray);

// for loops with array

for(let index = 0; index < newArray.length; index++){
	console.log(newArray[index]);
}

let numbers = [5, 12, 30, 46, 40];

for(let index = 0; index < numbers.length; index++){
	if(numbers[index] % 5 === 0){
		console.log(numbers[index] + " is divisible by 5.");
	}else{
		console.log(numbers[index] + " is not divisible by 5.");
	}
}


/*for(let index = 0; 0 < 5; index++){
	if(numbers[0] % 5 === 0){
		console.log(numbers[0] + " is divisible by 5.");
	}else{
		console.log(numbers[0] + " is not divisible by 5.");
	}
}*/
// 5 is divisible by 5.

/*for(let index = 1; 1 < 5; index++){
	if(numbers[1] % 5 === 0){
		console.log(numbers[1] + " is divisible by 5.");
	}else{
		console.log(numbers[1] + " is not divisible by 5.");
	}
}
*/
// 12 is not divisible by 5.

/*for(let index = 2; 2 < 5; index++){
	if(numbers[2] % 5 === 0){
		console.log(numbers[2] + " is divisible by 5.");
	}else{
		console.log(numbers[2] + " is not divisible by 5.");
	}
}*/
// 30 is divisible by 5.


/*................*/


/*for(let index = 5; 5 < 5; index++){
	if(numbers[5] % 5 === 0){
		console.log(numbers[5] + " is divisible by 5.");
	}else{
		console.log(numbers[5] + " is not divisible by 5.");
	}
}*/



