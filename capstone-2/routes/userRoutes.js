// [ SECTION ] Dependencies and Modules
const express = require('express');
const auth = require("../auth.js")
const {verify, verifyAdmin} = auth;
const router = express.Router();
const userController = require('../controllers/userControllers.js');


// check email routes
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// user registration routes
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// retrieve user details
router.get("/details", verify, userController.getProfile);

// Book user to a destination
router.post("/book", verify, userController.book);

// Get Logged User's Bookings
    router.get('/getBookings', verify, userController.getBookings)

// Reset Password
    router.put('/reset-password', verify, userController.resetPassword);   

// Update Profile  
    router.put('/profile', verify, userController.updateProfile);

// [ SECTION ] Export Route System
module.exports = router;
