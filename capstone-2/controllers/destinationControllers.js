const Destination = require("../models/Destination.js");

module.exports.addDestination = (req, res) => {
	let newDestination = new Destination({
		name : req.body.name,
		photos: req.body.photos,
		ratePerPax: req.body.ratePerPax,
		location: req.body.location,
		// currentBookings: req.body.currentBookings,
		description: req.body.description
	});

	return newDestination.save().then((destination, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
}
 
module.exports.getAllDestinations = (req, res) => {
	return Destination.find({}).then(result => {
		 return res.send(result);
        })
        .catch(err => res.send(err))
    };

module.exports.getAllActive = (req, res) => {
	return Destination.find({isActive : true}).then(result => {
		return res.send(result);
        })
        .catch(err => res.send(err))
    };

module.exports.getDestination = (req, res) => {
	return Destination.findById(req.params.destinationId).then(result =>{
		return res.send(result);
        })
        .catch(err => res.send(err))
    };

module.exports.updateDestination = (req, res) => {
	let updatedDestination = {
		name: req.body.name,
		description: req.body.description,
		location: req.body.location,
		ratePerPax: req.body.ratePerPax
	}

	return Destination.findByIdAndUpdate(req.params.destinationId, updatedDestination).then((destination, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
};


module.exports.archiveDestination = (req, res) => {
	let updateActiveField = {
		isActive: false
	}

	return Destination.findByIdAndUpdate(req.params.destinationId, updateActiveField).then((destination, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(err));
};


module.exports.activateDestination = (req, res) => {
	let updateActiveField = {
		isActive: true
	}

	return Destination.findByIdAndUpdate(req.params.destinationId, updateActiveField).then((destination, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
};


module.exports.archives = (req, res) => {
	return Destination.find({isActive: false}).then(result => {
		if(result.length === 0){
			return res.send("There is no tour destination in the archives.");
		}else{
			return res.send(result);
		}
	})
	.catch(error => res.send("zzz"));
};

module.exports.searchDestinationsByName = async (req, res) => {
    try {
      const { destinationName } = req.body;
  
      // Use a regular expression to perform a case-insensitive search
      const destinations = await Destination.find({
        name: { $regex: destinationName, $options: 'i' }
      });
  
      res.json(destinations);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
}