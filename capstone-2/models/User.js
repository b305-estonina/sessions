// [ SECTION ] Dependecies and Modules
const mongoose = require('mongoose');

// [ SECTION ] Schema/Blueprint
const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
		required: [true, 'First Name is required!']
	},
	lastName : {
		type: String,
		required: [true, 'Last Name is required!']
	},
	email: {
		type: String,
		required: [true, 'Email is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	}, 
	isAdmin: {
		type: Boolean,
		default: false	
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile No. is required!']
	},
	bookings: [
		{
		       destinationId: {
		           type: String,
		           required: [ true, 'Booking No. is required!']
		       },
		       destinationName: {
		           type: String,
		           required: [true, 'Destination Name is required!'] 
		       },
		       destinationDescription: {
		           type: String,
		           required: [true, 'Destination Description is required!'] 
		       },
		       destinationLocation: {
		           type: String,
		           required: [true, 'Destination Location is required!'] 
		       },
		       destinationRatePerPax: {
		           type: Number,
		           required: [true, 'Price per pax is required!'] 
		       },
		       bookOn: {
		           type: Date,
		           default: new Date()
		       },
		       status: {
		           type: String,
		           default: "Booked"
		       }
		   }
	]
})

// [ SECTION ] Model
module.exports = mongoose.model('User', userSchema);
	