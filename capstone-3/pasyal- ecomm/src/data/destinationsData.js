const destinationsData = [
	{
		photos: [
			"https://tieza.gov.ph/wp-content/uploads/2020/01/ams-bidr-01.jpg",
			"https://www.google.com/url?sa=i&url=https%3A%2F%2Ftieza.gov.ph%2Fbalicasag-island-dive-resort%2F&psig=AOvVaw0Mk0rjAm_2-o4EnGAU5zxB&ust=1696944181549000&source=images&cd=vfe&opi=89978449&ved=0CBEQjRxqFwoTCMCC9-qH6YEDFQAAAAAdAAAAABAJ",
			"https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.divephil.com%2Fbalicasag&psig=AOvVaw0Mk0rjAm_2-o4EnGAU5zxB&ust=1696944181549000&source=images&cd=vfe&opi=89978449&ved=0CBEQjRxqFwoTCMCC9-qH6YEDFQAAAAAdAAAAABAR"
			],
		id: "P001",
		name: "Panglao Island Tour",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque et nibh in mattis. In facilisis eros venenatis, aliquet ex eget, euismod dolor. Aenean ut sapien porta, pretium lacus at, ullamcorper nulla. Maecenas egestas dictum tellus, a malesuada velit auctor et. Nulla facilisi. Sed sodales ut ante sed blandit. Cras non neque risus. In pulvinar, ligula eu iaculis porta, lorem ante interdum justo, sit amet suscipit mi lectus vitae massa.",
		ratePerPax: 3500,
		location: "Bohol",
		onOffer: true
	},
	{
		id: "P002",
		name: "Whaleshark Viewing",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque et nibh in mattis. In facilisis eros venenatis, aliquet ex eget, euismod dolor. Aenean ut sapien porta, pretium lacus at, ullamcorper nulla. Maecenas egestas dictum tellus, a malesuada velit auctor et. Nulla facilisi. Sed sodales ut ante sed blandit. Cras non neque risus. In pulvinar, ligula eu iaculis porta, lorem ante interdum justo, sit amet suscipit mi lectus vitae massa.",
		ratePerPax: 2500,
		location: "Cebu",
		onOffer: true
	}
]
	export default destinationsData;