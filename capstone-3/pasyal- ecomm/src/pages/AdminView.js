import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

import EditDestination from '../components/EditDestination';
import ArchiveDestination from '../components/ArchiveDestination';


export default function AdminView({ destinationsData, fetchData }) {


    const [destinations, setDestinations] = useState([])


    useEffect(() => {
        const destinationsArr = destinationsData.map(destination=> {
            return (
                <tr key={destination._id}>
                    <td>{destination._id}</td>
                    <td>{destination.name}</td>
                    <td>{destination.description}</td>
                    <td>{destination.location}</td>
                    <td>{destination.ratePerPax}</td>
                    <td className={destination.isActive ? "text-success" : "text-danger"}>
                        {destination.isActive ? "Available" : "Unavailable"}
                    </td>
                    <td> <EditDestination destination={destination._id} fetchData={fetchData} /> </td> 
                    <td><ArchiveDestination destination={destination._id} isActive={destination.isActive} fetchData={fetchData}/></td>
                </tr>
                )
        })

        setDestinations(destinationsArr)

    }, [destinationsData])


    return(
        <>
            <h1 className="text-center my-4"> Admin Dashboard</h1>
            
            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Location</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th colSpan="2">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {destinations}
                </tbody>
            </Table>    
        </>

        )
}