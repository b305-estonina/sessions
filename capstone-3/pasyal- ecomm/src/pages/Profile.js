import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Tab, Tabs, Button } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import ResetPassword from '../components/ResetPassword';
import '../App.css'

export default function Profile() {
  const { user } = useContext(UserContext);
  const [details, setDetails] = useState({});
  const [bookings, setBookings] = useState([]);

  useEffect(() => {
    fetch(`https://capstone2-estonina.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data._id) {
          setDetails(data);
        }
      });

    fetch(`https://capstone2-estonina.onrender.com/users/getBookings`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        setBookings(data);
      });
  }, []);

  if (!user.id) {
    return <Navigate to="/profile" />;
  }

  return (
    <>
      <Tabs defaultActiveKey="profile" id="fill-tab-example" className="mb-3 tabs" fill>
        <Tab eventKey="profile" title="My Profile">
          <Row>
            <Col className="m-4 p-5 bg-dark text-white">
              <h2 className="mt-3">{`${details.firstName} ${details.lastName}`}</h2>
              <hr />
              <h4>Contacts</h4>
              <ul>
                <li>Email: {details.email}</li>
                <li>Mobile No: {details.mobileNo}</li>
              </ul>
              <div className="row d-inline-flex">
                <Link className="btn btn-success mb-3" to="/users/profile">
                  Update Profile
                </Link>
                <Link className="btn btn-danger" to="/users/reset-password">
                  Reset Password
                </Link>
              </div>
            </Col>
          </Row>
        </Tab>

        <Tab eventKey="bookings" title="My Bookings">
          <Row>
            <Col className="m-4 p-3">
              {bookings.map(booking => (
                <div key={booking.destinationId} className="mb-4 p-4 bg-primary" >
                  <p>Booked On: {new Date(booking.bookOn).toLocaleDateString()}</p>
                  {booking.destinationName && <p>Destination Name: {booking.destinationName}</p>}
                  {booking.destinationDescription && <p>Description: {booking.destinationDescription}</p>}
                  {booking.destinationLocation && <p>Location: {booking.destinationLocation}</p>}                 
                  {booking.destinationRatePerPax && <p>Price/  Pax: {booking.destinationRatePerPax}</p>}
                  <p>Status: {booking.status == 'Booked' ? 'CONFIRMED' : 'CANCELLED'}</p>

                  <div className= "d-flex ms-auto">
                      <button className='btn btn-danger d-flex'> CANCEL BOOKING </button>
                  </div>

                </div>
              ))}
            </Col>
          </Row>
        </Tab>
      </Tabs>
    </>
  );
}
