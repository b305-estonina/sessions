import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col, Carousel} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function DestinationView(){

	const {user} = useContext(UserContext);

	const {destinationId} = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [ratePerPax, setRatePerPax] = useState(0);
	const [photos, setPhotos]= useState([]);
	const [location,setLocation] = useState("");

	const myImageStyle = { width: '300', height: '180' }

	const book = (destinationId) => {
		fetch(`https://capstone2-estonina.onrender.com/users/book`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				destinationId: destinationId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data.message !== true){
				Swal.fire({
					title: "Successfully Booked!",
					icon: "success",
					text: "You have successfully booked for this beautiful destination."
				})

				navigate("/destinations");
			}else{
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
			console.log(data)	
			}
		})
	}

	useEffect(() => {
		console.log(destinationId);

		fetch(`https://capstone2-estonina.onrender.com/destinations/${destinationId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setRatePerPax(data.ratePerPax);
			setPhotos(data.photos);
			setLocation(data.location);

		})

	}, [destinationId])


	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 7, offset: 3 }}>
                    <Card>
                    	<Carousel>
                    	      <Carousel.Item>
                    	        <img className="img-fluid" src= {photos [0]} style= {myImageStyle}/> 
                    	      </Carousel.Item>
                    	      <Carousel.Item>
                    	        <img className="img-fluid" src= {photos [1]} style= {myImageStyle}/> 
                    	      </Carousel.Item>
                    	      <Carousel.Item>
                    	        <img className="img-fluid" src= {photos [2]}style= {myImageStyle} /> 
                    	      </Carousel.Item>
                    	</Carousel>

                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Location:</Card.Subtitle>
                            <Card.Text>{location}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {ratePerPax} /pax</Card.Text>

                            {
                            	user.id !== null ?
                            	<Button variant="primary" onClick={() => book(destinationId)}>Book Now</Button>
                            	:
                            	<Link className="btn btn-danger btn-block" to="/login">Login to Book</Link>
                            }               
                        </Card.Body>        
                   
                    </Card>
                </Col>
            </Row>
        </Container>
		)
}