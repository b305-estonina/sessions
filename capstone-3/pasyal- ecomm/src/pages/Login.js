import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from "../UserContext";
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2'

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    
    const [isActive, setIsActive] = useState(true);

	useEffect(() => {

        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password]);

	function authenticate(e) {

        e.preventDefault();
		fetch('https://capstone2-estonina.onrender.com/users/login',{

		method: 'POST',
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({

			email: email,
			password: password

		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(typeof data.access !== "undefined"){

			localStorage.setItem('token', data.access);

			// function for retrieving user details
			retrieveUserDetails(data.access);

			Swal.fire({
				title: "Login Successful!",
				icon: "success",
				text: "Browse.Book.Travel!"
			})

			
		} else {

			Swal.fire({
				title: "Authentication Failed!",
				icon: "error",
				text: "Check your login details and try again!"
			})
		}
	})

	setEmail('');
	setPassword('');
    }

    const retrieveUserDetails = (token) => {
    	fetch("https://capstone2-estonina.onrender.com/users/details", {
    		headers: {
    			Authorization: `Bearer ${token}`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {
    		console.log(data);

    		localStorage.setItem("firstName", data.firstName)

    		setUser({
    			id: data._id,
    			isAdmin: data.isAdmin
    		})
    	})
    }


    return (
    	(user.id === null)?
  	  	
	    <Form onSubmit={(e) => authenticate(e)}>
	    	<h1 className="my-5 text-center">Login</h1>
	        <Form.Group controlId="userEmail">
	            <Form.Label>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                required
	                value={email}
	    			onChange={(e) => setEmail(e.target.value)}
	            />
	        </Form.Group>
	        <Form.Group controlId="password">
	            <Form.Label>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                required
	                value={password}
	    			onChange={(e) => setPassword(e.target.value)}
	            />
	        </Form.Group>

	        <Button variant="primary" type="submit" id="submitBtn">
	        	Submit
	        </Button>
	    </Form> 

	    :

	    <Navigate to="/destinations" />       
    )
}