import Banner from '../components/Banner';

export default function Home() {

    const data = {
    
        title: "PasyalPH Travel and Tours",
        content: "See the Philippines with your own two eyes",
       
        destination: "/destinations",
        label: "Get Started"
    }

    return (
        <>
            <Banner data={data}/>
        </>
    )
}