import { useEffect, useState, useContext } from 'react';
// import DestinationCard from '../components/DestinationCard';
import destinationsData from '../data/destinationsData';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../pages/AdminView';

export default function Destinations() {

    const { user } = useContext(UserContext);
    const [destinations, setDestinations] = useState([]);


    const fetchData = () => {
        fetch(`https://capstone2-estonina.onrender.com/destinations/all`)
        .then(res => res.json())
        .then(data => {
            
            console.log(data);
            setDestinations(data);

        });
    }

    useEffect(() => {

        fetchData()

    }, []);

    return(
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView destinationsData={destinations} fetchData={fetchData} />

                    :

                    <UserView destinationsData={destinations} />

            }
        </>
        // <UserView destinationsData={destinations} />

    )
}