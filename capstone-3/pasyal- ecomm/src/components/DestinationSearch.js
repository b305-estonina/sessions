import React, { useState } from 'react';
import DestinationCard from './DestinationCard';

const DestinationSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://capstone2-estonina.onrender.com/destinations/searchByName', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ destinationName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for destinations:', error);
    }
  };

  return (
    <div className='pt-5 container bg-p'>
      <h2>Destination Search</h2>
      <div className="form-group">
        <label htmlFor="destinationName">Destination Name:</label>
        <input
          type="text"
          id="destinationName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.map(destination => (
        
          <div className="p-3 bg-success">
        <DestinationCard destinationProp={destination} key={destination._id}/>
        </div>
        ))}
      </ul>
    </div>
  );
};

export default DestinationSearch;