import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveDestination({destination, isActive, fetchData}) {

    const archiveToggle = (destinationId) => {
        fetch(`https://capstone2-estonina.onrender.com/destinations/${destinationId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Course successfully disabled'
                })
                fetchData();

            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    }


        const activateToggle = (destinationId) => {
        fetch(`https://capstone2-estonina.onrender.com/destinations/${destinationId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Course successfully enabled'
                })
                fetchData();
            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    }
 

    return(
        <>
            {isActive ?

                <Button variant="danger" size="sm" onClick={() => archiveToggle(destination)}>Archive</Button>

                :

                <Button variant="success" size="sm" onClick={() => activateToggle(destination)}>Activate</Button>

            }
        </>

        )
}