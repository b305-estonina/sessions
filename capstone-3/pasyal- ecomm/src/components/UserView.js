import React, { useState, useEffect } from 'react';
import DestinationCard from './DestinationCard';
import DestinationSearch from './DestinationSearch';


export default function UserView({destinationsData}) {

    const [destinations, setDestinations] = useState([])

    useEffect(() => {
        const destinationsArr = destinationsData.map(destination=> {
            
            if(destination.isActive === true) {
                return (
                    <DestinationCard destinationProp={destination} key={destination._id}/>
                    )
            } else {
                return null;
            }
        })
        
        setDestinations(destinationsArr)

    }, [destinationsData])

    return(
        <>
        
            <DestinationSearch />
            { destinations }
        </>
        )
} 