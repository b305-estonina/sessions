import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

    return (

        <div  className= "banner">
            <Row>
                <Col className="banner-title text-center">
                    <h1>{title}</h1>
                    <p><i>"{content}"</i></p>
                    <Link className="btn btn-outline-primary" to={destination}>{label}</Link>
                </Col>
            </Row>
        </div>    
    )
}