import React, { useState, useEffect, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import UserContext from '../UserContext';
import UpdateProfileForm from '../components/UpdateProfile';

function NavBar() {
  const { user } = useContext(UserContext);
  const [details, setDetails] = useState({});

  useEffect(() => {
    fetch(`https://capstone2-estonina.onrender.com/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (typeof data._id !== 'undefined') {
          setDetails(data);
        }
      });
  }, []);

  return (
    <Navbar bg="dark" data-bs-theme="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand className="text-white" href="/">
          PasyalPH
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="d-flex justify-content-end ms-auto">
            {user.id !== null ? (
              user.isAdmin ? (
                <>
                  <Nav.Link as={Link} to="/addDestination" className="text-white">
                    Add Destination
                  </Nav.Link>
                  <Nav.Link as={Link} to="/logout" className="text-white">
                    Logout
                  </Nav.Link>
                </>
              ) : (
                <Dropdown align="end">
                  <Dropdown.Toggle variant="success" id="dropdown-basic">
                    {localStorage.getItem("firstName")}
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Nav.Link as={Link} to="/profile">
                      My Profile
                    </Nav.Link>
                    <Nav.Link href="/destinations">Destinations</Nav.Link>
                    <Nav.Link as={Link} to="/logout">
                      Logout
                    </Nav.Link>
                  </Dropdown.Menu>
                </Dropdown>
              )
            ) : (
              <>
                <Nav.Link className="text-white" href="/destinations">
                  Destinations
                </Nav.Link>
                <Nav.Link className="text-white" as={Link} to="/register">
                  Register
                </Nav.Link>
                <Nav.Link className="text-white" as={Link} to="/login">
                  Login
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavBar;
