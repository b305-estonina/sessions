import {Button, Modal, Form} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';

export default function EditDestination({destination}){

	// state for course id which will be used for fetching
	const [destinationId, setDestinationId] = useState("");

	// state for editcourse modal
	const [showEdit, setShowEdit] = useState(false);

	// useState for our form (modal)
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [location,setLocation] = useState("");
	const [ratePerPax, setRatePerPax] = useState("");

	// function for opening the edit modal

	const openEdit = (destinationId) => {
		setShowEdit(true);

		fetch(`https://capstone2-estonina.onrender.com/destinations/${destinationId}`)
		.then(res => res.json())
		.then(data => {
			setDestinationId(data._id);
			setName(data.name);
			setDescription(data.description);
			setLocation(data.location);
			setRatePerPax(data.ratePerPax);
		})
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDescription("");
		setLocation("");
		setRatePerPax(0);
	}

	// function to save our update
	const editDestination = (e, destinationId) => {
		e.preventDefault();

		fetch(`https://capstone2-estonina.onrender.com/destinations/${destinationId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				location: location,
				ratePerPax: ratePerPax
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Update Success!",
					icon: "success",
					text: "Tourist Destination Successfully Updated!"
				})

				closeEdit();
			}else{
				Swal.fire({
					title: "Update Error!",
					icon: "error",
					text: "Please try again!"
				})
				closeEdit();
			}

		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => {openEdit(destination)}}>Edit</Button>

			 <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editDestination(e, destinationId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <Form.Group controlId="destinationName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" required value={name} onChange={e => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="destinationDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" required value={description} onChange={e => setDescription(e.target.value)}/>
                        </Form.Group>
                         <Form.Group controlId="destinationLocation">
                            <Form.Label>Location</Form.Label>
                            <Form.Control type="text" required value={location} onChange={e => setLocation(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="destinationRatePerPax">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" required value={ratePerPax} onChange={e => setRatePerPax(e.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
		</>

		)
}