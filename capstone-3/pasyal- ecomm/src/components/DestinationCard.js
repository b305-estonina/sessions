import { Card, Button, Col, Container, Row } from 'react-bootstrap';
import destinationsData from '../data/destinationsData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';


export default function DestinationCard({destinationProp}) {

	const { _id, name, description, ratePerPax, location, photos } = destinationProp;
	const myImageStyle = { width: '20rem', height: '10rem',}

	return (
		<Container className="mt-5">
            <Row>
				<Col lg={{ span: 6, offset: 4 }}>
					<Card className='mt-3'style= {{width: '20rem'}} >
						<Card.Img variant="top-center" src= {photos[0]} style= {myImageStyle} />
					    <Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price per Pax:</Card.Subtitle>
					        <Card.Text>PhP {ratePerPax}</Card.Text>
					        <Card.Subtitle>Location:</Card.Subtitle>
					        <Card.Text>{location}</Card.Text>
					        <Link className="btn btn-outline-primary" to={`/destinations/${_id}`}>View Details</Link>
					    </Card.Body>
					</Card>	
				</Col>
			</Row>
        </Container>	
	)
}

DestinationCard.propTypes = {
	destination: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		ratePerPax: PropTypes.number.isRequired,
		location: PropTypes.string.isRequired
	})
}