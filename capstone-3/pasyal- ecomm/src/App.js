import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NavBar from './components/NavBar';
import ResetPassword from './components/ResetPassword';
import UpdateProfile from './components/UpdateProfile';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import Home from './pages/Home';
import Destinations from './pages/Destinations';
import DestinationView from './pages/DestinationView';
import Register from './pages/Register';
import AddDestination from './pages/AddDestination';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Error from './pages/Error';


import './App.css';
import { UserProvider } from './UserContext';

function App() {

    const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
 
    const unsetUser = () => {

      localStorage.clear();

    };
    
    useEffect(() => {

    // console.log(user);
    fetch(`http://localhost:4000/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      
      if (typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });

     
      } else {

        setUser({
          id: null,
          isAdmin: null
        });

      }

    })

    }, []);


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
        <NavBar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/destinations" element={<Destinations />} />
            <Route path="/destinations/:destinationId" element={<DestinationView />}/>
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/addDestination" element={<AddDestination />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/users/reset-password" element={<ResetPassword />} />
            <Route path="/users/profile" element={<UpdateProfile />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>  
    </Router>
    </UserProvider>
  );
}

export default App;
