const User = require("../models/User.js");
const Destination = require("../models/Destination.js");
const bcrypt = require('bcrypt');
const auth = require('../auth');

//check-email
module.exports.checkEmailExist = (reqBody) => {
	console.log(reqBody);
	console.log(reqBody.email);
	return User.find({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result.length > 0){
			return (`Your email, ${reqBody.email} is registered already!`)
		} else {
			return (`${reqBody.email} is not yet registered!`)
		};
	});
};

//user registration
module.exports.registerUser = (reqBody) => {
	console.log(reqBody);
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10),
		email: reqBody.email
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false			
		} else {
			return true
		}
	})
	.catch(err => err);
};

// user authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result === null){
			return res.send(false);
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return res.send(false); // Passwords do not match
			}
		}
	})
	.catch(err => err)
}	

// Retrieve user details
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "********";
		return res.send(result);
	})
	.catch(error => error)
}

// Book user to a destination
module.exports.book = async (req, res) => {
    try {
        const { destinationId, destinationName, destinationDescription, destinationLocation, destinationRatePerPax } = req.body;

        // Update user
        const updatedUser = await User.findByIdAndUpdate(
            req.user.id,
            {
                $push: {
                    bookings: {
                        destinationId,
                        destinationName,
                        destinationDescription,
                        destinationLocation,
                        destinationRatePerPax,
                        bookOn: new Date(),
                        status: 'Booked'
                    }
                }
            },
            { new: true }
            
        );

        // Update destination
        const updatedDestination = await Destination.findByIdAndUpdate(
            destinationId,
            {
                $push: {
                    tourists: { userId: req.user.id }
                }
            },
            { new: true }
        );

        if (updatedUser && updatedDestination) {
            return res.status(200).json({ success: true, message: 'Booking successful', data: { user: updatedUser, destination: updatedDestination } });
        } else {
            return res.status(500).json({ success: false, message: 'Booking failed' });
        }
    } catch (error) {
        console.error(error);
        return res.status(500).json({ success: false, message: 'Internal server error' });
    }
};

// Get Bookings
  module.exports.getBookings = (req, res) => {
    User.findById(req.user.id)
        .populate({
            path: 'bookings.destinationId',
            select: ['destinationName', 'destinationDescription', 'destinationLocation', 'destinationRatePerPax']
        })
        .then(result => res.send(result.bookings))
        .catch(err => res.send(err))
}
// Reset Password
  module.exports.resetPassword = async (req, res) => {
        try {

        //console.log(req.user)
        //console.log(req.body)

          const { newPassword } = req.body;
          const { id } = req.user; // Extracting user ID from the authorization header
      
          // Hashing the new password
          const hashedPassword = await bcrypt.hash(newPassword, 10);
      
          // Updating the user's password in the database
          await User.findByIdAndUpdate(id, { password: hashedPassword });
      
          // Sending a success response
          res.status(200).send({ message: 'Password reset successfully' });
        } catch (error) {
          console.error(error);
          res.status(500).send({ message: 'Internal server error' });
        }
    };  



 module.exports.updateProfile = async (req, res) => {
        try {

            console.log(req.user);
            console.log(req.body);
            
        // Get the user ID from the authenticated token
          const userId = req.user.id;
      
          // Retrieve the updated profile information from the request body
          const { firstName, lastName, mobileNo } = req.body;
      
          // Update the user's profile in the database
          const updatedUser = await User.findByIdAndUpdate(
            userId,
            { firstName, lastName, mobileNo },
            { new: true }
          );
      
          res.send(updatedUser);
        } catch (error) {
          console.error(error);
          res.status(500).send({ message: 'Failed to update profile' });
        }
      }      