const express = require("express");
const destinationController = require("../controllers/destinationControllers.js");

const auth = require("../auth.js");

// destructing of verify and verifyAdmin

const {verify, verifyAdmin} = auth;

const router = express.Router();

// Create a course POST
router.post("/",  verify, verifyAdmin, destinationController.addDestination);

// Get all destinations
router.get("/all", destinationController.getAllDestinations);

// Get all active destinations
router.get("/", destinationController.getAllActive);

// Get 1 specific destination using its ID
router.get("/:destinationId", destinationController.getDestination);

// Updating a Destination (Admin Only)
router.put("/:destinationId", verify, verifyAdmin, destinationController.updateDestination);

// Archive a destination
router.put("/:destinationId/archive", verify, verifyAdmin, destinationController.archiveDestination);

//Un-archive/ Activate a destination
router.put("/:destinationId/activate", verify, verifyAdmin, destinationController.activateDestination);

router.post("/archives", verify, verifyAdmin, destinationController.archives);

 router.post('/searchByName', destinationController.searchDestinationsByName);

module.exports = router; 