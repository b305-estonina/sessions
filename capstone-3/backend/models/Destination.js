// [ SECTION ] Dependecies and Modules
const mongoose = require('mongoose');

// [ SECTION ] Schema/Blueprint
const destinationSchema = new mongoose.Schema({
	name: {
		type:String,
		required: [true, 'Destination is required!']
	},
	description : {
		type: String,
		required: [true, 'Description is required!']
	},
	ratePerPax: {
		type: Number,
		required: [true, 'Price is required!']
	},
	isActive: {
		type: Boolean,
		default:true
	},
	photos : [],
	location: {
		type: String,
		required: [true, 'Location is required!']
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	tourists: [
		{
			userId: {
				type: String,
				required: [ true, 'UserId is required!']
			},
			bookedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

// [ SECTION ] Model
module.exports = mongoose.model('Destination', destinationSchema);