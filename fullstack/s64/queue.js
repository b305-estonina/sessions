let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    // add an item
    collection.push(item); 
    return collection;

}

function dequeue(){
    // remove an item
    if (collection.length > 0) {
        collection.shift();
    }
    return collection;
}

function front(){
    // get the front item
    if (collection.length > 0) {
        return collection[0]; 
    } else {
        return undefined;
    }
}

function size(){
    // get the size of the array
    return collection.length;
}

function isEmpty(){
    // check array if empty
     return collection.length === 0;
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};